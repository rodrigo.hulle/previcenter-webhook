export const BasicAuthentication = (req, res, next) => {
  const authHeader = req.headers.authorization;

  if (!authHeader) {
    res.status(401).set('WWW-Authenticate', `Basic realm="User Visible Realm", charset="UTF-8"`);
    const error = new Error(`Não autenticado`);
    error.statusCode = 401;
    next(error);
  } else {
    const [, encoded] = authHeader.split(' ');

    if (encoded !== process.env.BASIC_AUTHENTICATION) {
      res.status(401);
      const error = new Error(`Login/Senha incorretos`);
      error.statusCode = 401;
      next(error);
    }

    res.status(200);
  }

  next();
};
