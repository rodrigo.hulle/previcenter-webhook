import Queue from '../lib/Queue';

export default {
  async store(req, res) {
    const payload = req.body;

    // Adicionar job Banco de Dados na fila
    await Queue.add('SaveDatabase', { payload }, { delay: 86400000 });

    return res.json(payload);
  },
};
