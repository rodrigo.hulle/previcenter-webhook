import Queue from '../lib/Queue';

export default {
  async store(req, res) {
    const payload = req.body;

    // Valida o payload Hubspot presença do Header é suficiente
    // https://developers.hubspot.com/docs/api/webhooks/validating-requests

    //Se Válido
    await Queue.add('SaveDatabase', { payload });

    res.status(200);
    res.send();

    // Senão envia ao Sentry.
  },
};
