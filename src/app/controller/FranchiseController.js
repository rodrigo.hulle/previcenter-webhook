import prisma from '../lib/Prisma';

const updateOne = (data) => {
  return prisma.franquia.update({
    where: {
      nome: data.nome,
    },
    data: data,
  });
};

const deleteOne = (data) => {
  return prisma.franquia.delete({
    where: {
      nome: data.nome,
    },
  });
};

export default {
  async list(req, res) {
    const { active } = req.query;

    const args = {};
    if (active) {
      args.where = {
        ativo: true,
      };
    }

    const response = await prisma.franquia.findMany(args);

    return res.json(response);
  },

  async create(req, res) {
    const payload = req.body;

    if (Array.isArray(payload)) {
      await prisma.franquia.createMany({
        data: payload,
      });
    } else {
      await prisma.franquia.create({
        data: payload,
      });
    }

    return res.json(payload);
  },

  async update(req, res) {
    const payload = req.body;

    let updates = [];

    if (Array.isArray(payload)) {
      for (const franchise of payload) {
        updates.push(updateOne(franchise));
      }
    } else {
      updates.push(updateOne(payload));
    }

    await prisma.$transaction(updates);

    return res.json(payload);
  },

  async delete(req, res) {
    const payload = req.body;

    let deletes = [];

    if (Array.isArray(payload)) {
      for (const franchise of payload) {
        deletes.push(deleteOne(franchise));
      }
    } else {
      deletes.push(deleteOne(payload));
    }

    await prisma.$transaction(deletes);

    return res.json(payload);
  },
};
