import PrismaClientPackage from '@prisma/client';
import * as Sentry from '@sentry/node';

const { PrismaClient } = PrismaClientPackage;
const prisma = new PrismaClient();

Sentry.init({
  dsn: 'https://6a6e63dddb28468bb3c49d0d513b839e@o594846.ingest.sentry.io/5786609',
  tracesSampleRate: 1.0,
  environment: process.env.NODE_ENV,
});

prisma.$use(async (params, next) => {
  try {
    const before = Date.now();

    const result = await next(params);

    const after = Date.now();

    Sentry.withScope(async (scope) => {
      scope.setTag('job', 'SaveDatabase');

      scope.addBreadcrumb({
        type: 'info',
        category: 'prisma',
        level: Sentry.Severity.Info,
        data: {
          args: { ...params.args },
          dataPath: [...params.dataPath],
          runInTransaction: params.runInTransaction,
          action: params.action,
          model: params.model,
          time: `${after - before}ms`,
        },
      });
    });

    return result;
  } catch (err) {
    Sentry.withScope(async (scope) => {
      scope.setTag('prisma', params.action);

      scope.setContext('params', {
        args: { ...params.args },
        dataPath: [...params.dataPath],
        runInTransaction: params.runInTransaction,
        action: params.action,
        model: params.model,
      });

      Sentry.captureException(err);
    });
    return null;
  }
});

export default prisma;
