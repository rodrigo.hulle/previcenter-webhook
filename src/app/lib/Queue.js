import Queue from 'bull';
import redisConfig from '../../config/redis';
import * as Sentry from '@sentry/node';

import * as jobs from '../jobs';

Sentry.init({
  dsn: 'https://6a6e63dddb28468bb3c49d0d513b839e@o594846.ingest.sentry.io/5786609',
  tracesSampleRate: 1.0,
  environment: process.env.NODE_ENV,
});

console.log(redisConfig);
const queues = Object.values(jobs).map((job) => {
  return {
    bull: new Queue(job.key, redisConfig, job.queueOptions),
    name: job.key,
    handle: job.handle,
    options: job.jobOptions,
  };
});

const setContexts = (scope, job, result) => {
  scope.setContext('queue', {
    name: job.queue.name,
  });

  let payload = {};

  if (job.data && job.data.payload) {
    payload = { ...job.data.payload };
    delete payload.dependentes;
    job.data.payload.dependentes.forEach((d, index) => {
      payload[`dependente(${index})`] = { ...d };
    });
  }

  scope.setContext('payload', {
    ...payload,
  });

  scope.setContext('job', {
    ...job,
  });

  if (result) {
    scope.setContext('result', {
      ...result,
    });
  }
};

export default {
  queues,
  add(name, data, options) {
    const queue = this.queues.find((queue) => queue.name === name);
    return queue.bull.add(data, { ...queue.options, ...options });
  },
  pause(name) {
    const queue = this.queues.find((queue) => queue.name === name);
    return queue.bull.pause(true);
  },
  resume(name) {
    const queue = this.queues.find((queue) => queue.name === name);
    return queue.bull.resume(true);
  },
  clean(name, period, status, limit) {
    const queue = this.queues.find((queue) => queue.name === name);
    return queue.bull.clean(period, status, limit);
  },
  getCompletedCount(name) {
    const queue = this.queues.find((queue) => queue.name === name);
    return queue.bull.getCompletedCount();
  },
  process() {
    return this.queues.forEach((queue) => {
      queue.bull.process(queue.handle);

      // queue.bull.on('completed', (job, result) => {
      //   Sentry.withScope(function (scope) {
      //     scope.setTag('job', job.queue.name);

      //     setContexts(scope, job, result);

      //     Sentry.captureEvent({
      //       message: `Job completed`,
      //       level: 'info',
      //     });
      //   });
      // });

      queue.bull.on('failed', (job, err) => {
        Sentry.withScope(function (scope) {
          scope.setTag('job', job.queue.name);

          setContexts(scope, job);

          Sentry.captureException(err);
        });
      });
    });
  },
};
