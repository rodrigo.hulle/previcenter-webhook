import Queue from '../lib/Queue';

export default {
  key: 'DailyCardsLimit',

  jobOptions: {
    removeOnComplete: process.env.BULL_MAX_COMPLETED_COUNT ? parseInt(process.env.BULL_MAX_COMPLETED_COUNT) : 100,
    repeat: {
      cron: process.env.SCHEDULE_DAILY_CARDS_LIMIT,
      tz: 'America/Sao_Paulo',
    },
  },

  async handle() {
    await Queue.clean('SendPipefyCard', 500);
    await Queue.resume('SendPipefyCard');

    return { message: `Limite diário de cards resetado` };
  },
};
