import Queue from '../lib/Queue';
import prisma from '../lib/Prisma';

const createMatricula = (payload) => {
  return prisma.matricula.create({
    data: {
      franquia: payload.franquia,
      matricula: payload.matricula,
      pessoas: {
        createMany: {
          data: [
            {
              idPessoa: payload.titular.id,
              nome: payload.titular.nome,
              email: payload.titular.email ? payload.titular.email : null,
              telefone: payload.titular.telefone ? payload.titular.telefone : null,
              dataNascimento: new Date(payload.titular.dataNascimento),
              tipo: 'TITULAR',
            },
            ...payload.dependentes.map((dependente) => ({
              idPessoa: dependente.id,
              nome: dependente.nome,
              email: dependente.email ? dependente.email : null,
              telefone: dependente.telefone ? dependente.telefone : null,
              dataNascimento: new Date(dependente.dataNascimento),
              grauParentesco: dependente.grauParentesco,
              tipo: 'DEPENDENTE',
            })),
          ],
        },
      },
    },
  });
};

export default {
  key: 'SaveDatabase',

  jobOptions: {
    removeOnComplete: process.env.BULL_MAX_COMPLETED_COUNT ? parseInt(process.env.BULL_MAX_COMPLETED_COUNT) : 100,
  },

  async handle({ data }) {
    const { payload } = data;

    // Salva no Banco de dados a requisição e os metadados para replay.
    // Teremos um Job Mensal que manterá apenas os ultimos 90 dias.

    try {
      let matricula;

      switch (payload.evento) {
        case 'br.com.sistematodos.filiacao.comdependentes':
        case 'br.com.sistematodos.refiliacao.comdependentes':
          // Salva no banco a matrícula, o titular e os dependentes
          matricula = await prisma.matricula.findUnique({
            where: {
              matricula: payload.matricula,
            },
          });

          if (!matricula) {
            await createMatricula(payload);
          }
          break;
        case 'br.com.sistematodos.desfiliacao.comdependentes':
          // Remove do banco a matrícula, o titular e os dependentes
          matricula = await prisma.matricula.findUnique({
            where: {
              matricula: payload.matricula,
            },
          });

          if (!matricula) {
            return;
            // throw new Error(`Não foi possível remover a matrícula. Matrícula não existente no banco de dados.`);
          }

          const deletePessoas = prisma.pessoa.deleteMany({
            where: {
              idMatricula: matricula.id,
            },
          });

          const deleteMatricula = prisma.matricula.delete({
            where: {
              id: matricula.id,
            },
          });

          await prisma.$transaction([deletePessoas, deleteMatricula]);

          break;
        case 'br.com.sistematodos.dependente.adicionado':
          // Salva no banco o novo dependente vinculado à matrícula recebida no payload
          matricula = await prisma.matricula.findUnique({
            where: {
              matricula: payload.matricula,
            },
          });

          if (!matricula) {
            matricula = await createMatricula(payload);
          } else {
            if (payload.dependentes.length === 0) {
              throw new Error(`Não foi possível adicionar um novo dependente. Payload sem dependentes.`);
            }

            await prisma.pessoa.createMany({
              data: [
                ...payload.dependentes.map((dependente) => ({
                  idPessoa: dependente.id,
                  nome: dependente.nome,
                  email: dependente.email ? dependente.email : null,
                  telefone: dependente.telefone ? dependente.telefone : null,
                  dataNascimento: new Date(dependente.dataNascimento),
                  grauParentesco: dependente.grauParentesco,
                  idMatricula: matricula.id,
                  tipo: 'DEPENDENTE',
                })),
              ],
            });
          }

          break;
        case 'br.com.sistematodos.dependente.removido':
          // Remove do banco o dependente recebido no payload
          if (payload.dependentes.length === 0) {
            throw new Error(`Não foi possível remover o dependente. Payload sem dependentes.`);
          }

          await prisma.pessoa.deleteMany({
            where: {
              idPessoa: {
                in: payload.dependentes.map((d) => d.id),
              },
            },
          });

          break;
        default:
          throw new Error(`Evento inválido`);
      }

      await Queue.add('Evaluate', { payload });

      return;
    } catch (err) {
      throw err;
    }
  },
};
