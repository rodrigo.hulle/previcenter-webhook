import Queue from '../lib/Queue';
import prisma from '../lib/Prisma';

export default {
  key: 'Evaluate',

  jobOptions: {
    removeOnComplete: process.env.BULL_MAX_COMPLETED_COUNT ? parseInt(process.env.BULL_MAX_COMPLETED_COUNT) : 100,
  },

  async handle({ data }) {
    const { payload } = data;

    try {
      const activeFranchises = await prisma.franquia.findMany({
        select: {
          nome: true,
        },
        where: {
          ativo: true,
        },
      });
      const temp = activeFranchises.map((f) => f.nome);

      let isActive = false;
      let cardsParaEnviar = 0;
      if (temp.includes(payload.franquia)) {
        isActive = true;
        const now = new Date();
        const fiveYearsAgo = new Date(now.getFullYear() - 5, now.getMonth(), now.getDate());

        switch (payload.evento) {
          case 'br.com.sistematodos.filiacao.comdependentes':
          case 'br.com.sistematodos.refiliacao.comdependentes':
          case 'br.com.sistematodos.dependente.adicionado':
            // Em qualquer um desses três eventos são enviados cards ao Pipefy
            // para cada filho menor do que 5 anos presente no payload
            payload.dependentes.forEach(async (d) => {
              if (d.grauParentesco === 'Filho' && new Date(d.dataNascimento) >= fiveYearsAgo) {
                await Queue.add('SendPipefyCard', {
                  payload: {
                    ...payload,
                    idDependente: d.id,
                  },
                });
                cardsParaEnviar++;
              }
            });
            break;
          case 'br.com.sistematodos.desfiliacao.comdependentes':
          case 'br.com.sistematodos.dependente.removido':
            // Não é necessário avaliar a regra de negócio em uma desfiliação ou remoção de dependente
            break;
          default:
            throw new Error(`Evento inválido`);
        }
      }

      return { franquiaAtiva: isActive, cardsParaEnviar };
    } catch (err) {
      throw err;
    }
  },
};
