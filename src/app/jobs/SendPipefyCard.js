import Queue from '../lib/Queue';
import prisma from '../lib/Prisma';
import Pipefy from '../../drives/Pipefy';

const pipefy = new Pipefy();

const dailyLimit = process.env.DAILY_CARDS_LIMIT ? parseInt(process.env.DAILY_CARDS_LIMIT) : Number.MAX_SAFE_INTEGER;

export default {
  key: 'SendPipefyCard',

  queueOptions: {
    limiter: {
      max: 400,
      duration: 30000,
    },
  },

  jobOptions: {
    removeOnComplete: false,
  },

  async handle({ data }) {
    const { payload } = data;

    const now = new Date();
    const fiveYearsAgo = new Date(now.getFullYear() - 5, now.getMonth(), now.getDate());

    try {
      const updateID = payload.idDependente;

      const idMatricula = await prisma.matricula.findUnique({
        select: {
          id: true,
        },
        where: {
          matricula: payload.matricula,
        },
      });
      const count = await prisma.pessoa.count({
        where: {
          idPessoa: {
            not: {
              equals: updateID,
            },
          },
          grauParentesco: 'Filho',
          dataNascimento: {
            gte: fiveYearsAgo,
          },
          dataEnvio: null,
          idCard: null,
          idMatricula: idMatricula.id,
        },
      });
      const etiquetar = count > 0;

      const dependentes = payload.dependentes.map((d) => ({
        id: d.id,
        nome: d.nome,
        email: d.email,
        telefone: d.telefone,
        data_nascimento: d.dataNascimento,
        grau_parentesco: d.grauParentesco,
      }));
      const matricula = {
        id: payload.matricula,
        franquia: payload.franquia,
        etiquetar: etiquetar,
        idDependente: payload.idDependente,
        id_titular: payload.titular.id,
        nome_titular: payload.titular.nome,
        email_titular: payload.titular.email,
        telefone_titular: payload.titular.telefone,
        data_nascimento_titular: payload.titular.dataNascimento,
      };

      const card = await pipefy.run({ matricula, dependentes });
      try {
        await prisma.pessoa.update({
          where: {
            idPessoa: updateID,
          },
          data: {
            dataEnvio: new Date(),
            idCard: card.id,
          },
        });
      } catch (err) {
        await pipefy.deleteCard(card.id);
        throw err;
      }

      const completedCount = await Queue.getCompletedCount('SendPipefyCard');
      if (completedCount >= dailyLimit - 1) {
        Queue.pause('SendPipefyCard');
      }

      return { card };
    } catch (err) {
      throw err;
    }
  },
};
