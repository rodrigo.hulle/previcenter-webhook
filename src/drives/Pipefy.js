import axios from 'axios';

const pipefyGraphiql = axios.create({
  baseURL: 'https://app.pipefy.com/queries',
  headers: {
    'Content-Type': 'application/json',
    Authorization: `Bearer ${process.env.PIPEFY_TOKEN}`,
    credentials: 'include',
  },
});

pipefyGraphiql.interceptors.response.use(
  (response) => {
    return response.data;
  },
  (error) => {
    throw error.response.data;
  }
);

export default class Pipefy {
  config = null;

  constructor(config) {
    this.config = config;
  }

  run(data) {
    /**
     * Alguns componentes precisam de uma formatação em suas respostas para funcionarem corretamente no Pipefy.
     * Primeiro encontramos esses componentes e posteriormente tratamos seus dados no método pipefyFieldMappings
     */
    return new Promise(async (resolve, reject) => {
      let cardId;

      try {
        const matricula = { ...data.matricula };
        const { etiquetar: label, idDependente } = matricula;

        delete matricula.etiquetar;
        delete matricula.idDependente;

        const fields_attributes = this.pipefyFieldMappings(matricula);
        const card = await this.createCard(data.matricula.id, fields_attributes, label);

        cardId = card.id;

        const comments = this.pipefyRelativesCommentMapping(data.dependentes, idDependente);
        for (const comment of comments) {
          await this.createComment(card.id, comment);
        }

        resolve(card);
        resolve();
      } catch (err) {
        if (cardId) {
          // Ocorreu um erro durante a criação de comentários, portanto o card deve ser excluído
          try {
            // Exclusão do card
            await this.deleteCard(cardId);
          } catch (deleteError) {
            console.error(deleteError);
          }
        }
        reject(err);
      }
    });
  }

  formatFieldKey(key) {
    return key
      .replaceAll('id', 'ID')
      .split('_')
      .map((w) => w.charAt(0).toUpperCase() + w.slice(1))
      .join(' ');
  }

  pipefyFieldMappings(data) {
    /*
     * Formatando os campos do formulário de forma padronizada de acordo com o formato do GraphQL
     */
    let fields_attributes = '';
    for (const [key, value] of Object.entries(data)) {
      // Os únicos 3 valores possíveis são string e número

      if (typeof value === 'string' || typeof value === 'number') {
        fields_attributes += `{ field_id: "${key}", field_value: "${value}" },`;
      } else if (value instanceof Date) {
        fields_attributes += `{ field_id: "${key}", field_value: "${value.toString()}" }`;
      }
    }

    return fields_attributes;
  }

  pipefyRelativesCommentMapping(dependentes, idDependente) {
    let comments = [];

    const cardRelative = dependentes.find((d) => d.id === idDependente);
    const sorted = dependentes.filter((d) => d.id !== idDependente);
    sorted.push(cardRelative);

    sorted.forEach((dependente) => {
      let comment = '';

      for (const [key, value] of Object.entries(dependente)) {
        if (value) {
          if (comment.length !== 0) {
            comment += '\n';
          } else if (dependente.id === idDependente) {
            comment += '**FILHO MENOR DE 5 ANOS**\n\n';
          }

          if (typeof value === 'string' || typeof value === 'number') {
            comment += `**${this.formatFieldKey(key)}:** ${value}`;
          } else if (value instanceof Date) {
            let stringDate = `${dependente[key]
              .getDate()
              .toLocaleString('en-US', { minimumIntegerDigits: 2, useGrouping: false })}/`;

            stringDate += `${(dependente[key].getMonth() + 1).toLocaleString('en-US', {
              minimumIntegerDigits: 2,
              useGrouping: false,
            })}/`;

            stringDate += `${dependente[key]
              .getFullYear()
              .toLocaleString('en-US', { minimumIntegerDigits: 2, useGrouping: false })}`;

            comment += `**${this.formatFieldKey(key)}:** ${stringDate}`;
          }
        }
      }
      comments.push(comment);
    });

    return comments;
  }

  createCard(title, fields_attributes, label = false) {
    return new Promise(async (resolve, reject) => {
      try {
        const mutation = `mutation {
                            createCard (
                              input: {
                                pipe_id: ${process.env.PIPE_ID},
                                phase_id: "${process.env.PHASE_ID}",
                                title: "${title}",
                                fields_attributes: [${fields_attributes}],
                                label_ids: [${label ? process.env.LABEL_ID : ''}]
                              }
                            ),
                            {
                              card {
                                id,
                                title
                              }
                            }
                          }`;

        const response = await pipefyGraphiql.post('', JSON.stringify({ query: mutation }));

        if (response.errors && response.errors.length > 0) {
          reject(response.errors[0]);
        }

        resolve(response.data.createCard.card);
      } catch (err) {
        reject(err);
      }
    });
  }

  deleteAllCards() {
    return new Promise(async (resolve, reject) => {
      try {
        const allCardsMutation = `{
                                    allCards(pipeId:301575393){
                                      edges {
                                        node {
                                          id
                                        }
                                      }
                                    }
                                  }
                                  `;
        const allCards = await pipefyGraphiql.post('', JSON.stringify({ query: allCardsMutation }));

        for (const e of allCards.data.allCards.edges) {
          const deleteCardMutation = `mutation {
                                        deleteCard (
                                          input: {
                                            id: ${e.node.id}
                                          }
                                        ),
                                        {
                                          success
                                        }
                                      }`;

          const response = await pipefyGraphiql.post('', JSON.stringify({ query: deleteCardMutation }));

          if (response.errors && response.errors.length > 0) {
            reject(response.errors[0]);
          }
        }

        resolve();
      } catch (err) {
        reject(err);
      }
    });
  }

  deleteCard(cardId) {
    return new Promise(async (resolve, reject) => {
      try {
        const mutation = `mutation {
                            deleteCard (
                              input: {
                                id: ${cardId}
                              }
                            ),
                            {
                              success
                            }
                          }`;

        const response = await pipefyGraphiql.post('', JSON.stringify({ query: mutation }));

        if (response.errors && response.errors.length > 0) {
          reject(response.errors[0]);
        }

        resolve();
      } catch (err) {
        reject(err);
      }
    });
  }

  createComment(cardId, text) {
    return new Promise(async (resolve, reject) => {
      try {
        const mutation = `mutation {
                            createComment(
                              input: {
                                card_id: ${cardId},
                                text: "${text}"
                              }
                            ),
                            {
                              comment {
                                id,
                                text
                              }
                            }
                          }`;

        const response = await pipefyGraphiql.post('', JSON.stringify({ query: mutation }));

        if (response.errors && response.errors.length > 0) {
          reject(response.errors[0]);
        }

        resolve();
      } catch (err) {
        reject(err);
      }
    });
  }
}
