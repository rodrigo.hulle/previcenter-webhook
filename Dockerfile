FROM node:16.2

WORKDIR /

COPY package*.json ./
COPY nodemon.json ./
RUN yarn install

COPY /src /src

EXPOSE 3333
CMD [ "yarn", "prod" ]
