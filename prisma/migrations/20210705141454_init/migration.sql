-- CreateEnum
CREATE TYPE "TIPO_PESSOA" AS ENUM ('TITULAR', 'DEPENDENTE');

-- CreateTable
CREATE TABLE "Matricula" (
    "id" SERIAL NOT NULL,
    "franquia" TEXT NOT NULL,
    "matricula" TEXT NOT NULL,
    "criadoEm" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "atualizadoEm" TIMESTAMP(3) NOT NULL,

    PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "Pessoa" (
    "id" SERIAL NOT NULL,
    "idPessoa" INTEGER NOT NULL,
    "nome" TEXT,
    "email" TEXT,
    "telefone" TEXT,
    "dataNascimento" TIMESTAMP(3),
    "grauParentesco" TEXT,
    "idMatricula" INTEGER NOT NULL,
    "tipo" "TIPO_PESSOA" NOT NULL,

    PRIMARY KEY ("id")
);

-- CreateIndex
CREATE UNIQUE INDEX "Pessoa.idPessoa_unique" ON "Pessoa"("idPessoa");

-- AddForeignKey
ALTER TABLE "Pessoa" ADD FOREIGN KEY ("idMatricula") REFERENCES "Matricula"("id") ON DELETE CASCADE ON UPDATE CASCADE;
