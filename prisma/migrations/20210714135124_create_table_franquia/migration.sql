-- CreateTable
CREATE TABLE "Franquia" (
    "id" SERIAL NOT NULL,
    "nome" TEXT NOT NULL,
    "ativo" BOOLEAN NOT NULL DEFAULT true,

    PRIMARY KEY ("id")
);

-- CreateIndex
CREATE UNIQUE INDEX "Franquia.nome_unique" ON "Franquia"("nome");
