/*
  Warnings:

  - A unique constraint covering the columns `[matricula]` on the table `Matricula` will be added. If there are existing duplicate values, this will fail.

*/
-- CreateIndex
CREATE UNIQUE INDEX "Matricula.matricula_unique" ON "Matricula"("matricula");
